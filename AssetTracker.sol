pragma solidity 0.5.1;
 
contract AssetTracker {
    
    // Stages in the supply chain
    enum State { Raw_Material, Supplier, Manufacturer, Distributer, Retailer, Consumer}
    
    // Relief Resources
    struct Asset {
        string name;
        string demand;
        State stage;
        bool initialized;
    }
    
    // Relief Needs
    struct Demand {
        string name;
        string location;
        bool accepted;
        bool fulfilled;
    }
    
    // Default Permissions
    State user = State.Raw_Material;
    
    // Hashmaps
    mapping(string  => Asset) private assetStore;
    mapping(address => mapping(string => bool)) private walletStore;
    mapping(string => Demand) private demandStore;
    
    // Notification Events
    //https://asset-tracker.codecentric.de/
    event AssetCreate(address account, string uuid, State stage);
    event RejectCreate(address account, string uuid, string message);
    event AssetTransfer(address from, address to, string uuid);
    event RejectTransfer(address from, address to, string uuid, string message);
    event RejectUpdate(address account, string uuid, string message);
    
    // Create new Asset
    function createAsset(string memory name, string memory demand, string memory uuid) public {
 
        if(assetStore[uuid].initialized) {
            emit RejectCreate(msg.sender, uuid, "Asset with this UUID already exists.");
            return;
          }
     
          assetStore[uuid] = Asset(name, demand, State.Raw_Material, true);
          demandStore[demand].accepted = true;
          walletStore[msg.sender][uuid] = true;
          emit AssetCreate(msg.sender, uuid, State.Raw_Material);
    }
    
    // Identify new Needs
    function createDemand(string memory name, string memory location, string memory uuid) public {
        demandStore[uuid] = Demand(name, location, false, false);
    }
    
    
    // Transfer ownership of an asset
    function transferAsset(address to, string memory uuid) public {
 
        if(!assetStore[uuid].initialized) {
            emit RejectTransfer(msg.sender, to, uuid, "No asset with this UUID exists");
            return;
        }
     
        if(!walletStore[msg.sender][uuid]) {
            emit RejectTransfer(msg.sender, to, uuid, "Sender does not own this asset.");
            return;
        }
     
        walletStore[msg.sender][uuid] = false;
        walletStore[to][uuid] = true;
        emit AssetTransfer(msg.sender, to, uuid);
    }
    
    // print stage in supply chain of asset
    function printAssetStatus(string memory uuid) public view returns (string memory, State) {
        return (assetStore[uuid].name, assetStore[uuid].stage);
    }
    
    // print location and status of needs
    function printDemandStatus(string memory uuid) public view returns (string memory, string memory, bool, bool) {
        return (demandStore[uuid].name, demandStore[uuid].location, demandStore[uuid].accepted, demandStore[uuid].fulfilled);
    }
    
    //
    // Processes on the supply chain
    //
    function scanAssetSuplier(string memory uuid) public{
        if (user != State.Raw_Material){
            emit RejectUpdate(msg.sender, uuid, "Scanner does not have permission");
            return;
        }
        assetStore[uuid].stage = State.Supplier;
        buyToken();
    }
    
    function scanAssetManufacturer(string memory uuid) public{
        if (user != State.Supplier){
            emit RejectUpdate(msg.sender, uuid, "Scanner does not have permission");
            return;
        }
        assetStore[uuid].stage = State.Manufacturer;
        buyToken();
    }
    
    function scanAssetDistributer(string memory uuid) public {
        if (user != State.Manufacturer){
            emit RejectUpdate(msg.sender, uuid, "Scanner does not have permission");
            return;
        }
        assetStore[uuid].stage = State.Distributer;
        buyToken();
    }
    
    function scanAssetRetailer(string memory uuid) public{
        if (user != State.Distributer){
            emit RejectUpdate(msg.sender, uuid, "Scanner does not have permission");
            return;
        }
        assetStore[uuid].stage = State.Retailer;
        buyToken();
    }
    
    function scanAssetConsumer(string memory uuid) public {
        if (user != State.Retailer){
            emit RejectUpdate(msg.sender, uuid, "Scanner does not have permission");
            return;
        }
        assetStore[uuid].stage = State.Consumer;
        demandStore[assetStore[uuid].demand].fulfilled = true;
        buyToken();
    }
    
    //
    // End Processes
    //
    
    function updateScannerRole(State state) public {
        user = state;
    }
    
    function isOwnerOf(address owner, string memory uuid) public view returns (bool) {
 
        if(walletStore[owner][uuid]) {
            return true;
        }
     
        return false;
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //                              FINANCIALS
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    mapping(address => uint256) public balances;

    address payable wallet;

    event Purchase(
        address indexed _buyer,
        uint256 _amount
    );

    constructor(address payable _wallet) public {
        wallet = _wallet;
    }

    function() external payable {
        buyToken();
    }

    function buyToken() public payable {
        balances[msg.sender] += 1 ether;
        wallet.transfer(msg.value);
        emit Purchase(msg.sender, 1);
    }
}
